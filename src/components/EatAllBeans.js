import React from 'react';
import {useEffect} from "react";



const EatAllBeans = (props) => {

    let beanCount = props.beanCount;
    let setBeanCount = props.setBeanCount;
    let beansConsumed = props.beansConsumed;
    let setBeansConsumed = props.setBeansConsumed;
    let healthActive = props.healthActive;
    let setHealthActive = props.setHealthActive;

    useEffect(() => {
        console.log('Health is active');
    }, [healthActive]);

    return (
        <div>
            <div className='counterContainer'>
                {beanCount >=1 
                    ? 
                    <button className='counterButton' onClick={() => {
                        setHealthActive(true);
                        setBeansConsumed(beansConsumed + beanCount);
                        (setBeanCount(0))
                    }}>
                        <p>Eat all the beans</p>
                    </button> 
                    : null}              
            </div>
        </div>
    );
};





export default EatAllBeans;