import React from 'react';
import {useEffect} from 'react';

const OnGround = (props) => {

    let beanCount = props.beanCount;
    let setBeanCount = props.setBeanCount;
    let groundCount = props.groundCount
    let setGroundCount = props.setGroundCount
    let onGroundActive = props.onGroundActive;
    let setOnGroundActive = props.setOnGroundActive;
    
    // clear counter dependancies

    let clearCounter = props.clearCounter;
    let setClearCounter = props.setClearCounter;
    // checks to see if the number of beans on the ground has reached a certain number. if greater than 50, bean count is set to zero and default values are reset
    // set a timeout on ground active(doesnt have any text) so you can see the paragraph - a crow ate all your beans 
     
    useEffect(() => {
        groundCount >= 50 ? setClearCounter(true) : setClearCounter(false);
        if (clearCounter) {
            // eslint-disable-next-line
            setBeanCount(beanCount = 0);
            setOnGroundActive(false);
            setTimeout(() => {
                // eslint-disable-next-line
                setGroundCount(groundCount = 0);
            }, 3000);       
            }   
        })

    useEffect(() => {
            console.log('thrown on the ground button pressed!');
        }, [onGroundActive]);
    
    
    return (
        <div>
            <div className='counterContainer'>
                {beanCount >= 10
                        ?
                        <div>
                            <button className='counterButton' onClick={() => {
                                setBeanCount(beanCount -=10);
                                setGroundCount(groundCount +=10);
                                setOnGroundActive(true)
                            }}>
                                Throw 10 on the ground
                            </button>
                        </div>
                        : null
                    }
            </div>
            
            
        </div>
    );
};

export default OnGround;