import React from 'react';
import {useEffect} from 'react';



const Farm = (props) => {

    let sproutCount = props.sproutCount;
    let setSproutCount = props.setSproutCount
    // eslint-disable-next-line
    let beanCount = props.beanCount;
    let setBeanCount = props.setBeanCount;

    let plantedSprouts = props.plantedSprouts;
    let setPlantedSprouts = props.setPlantedSprouts;
    let farmActive = props.farmActive;
    let setFarmActive = props.setFarmActive;

    // sets the timer to run once per secon. then it gets cleared. I think this works cos use effect is always running so it just restarts it again???

    useEffect(() => {
        let myInterval = setInterval(() => {
            setBeanCount((beanCount) => beanCount + plantedSprouts)
        }, 1000)
        return () => clearInterval(myInterval);
    })

    useEffect(() => {
        console.log('plant a sprout button pressed!');
    }, [farmActive]);

    return (
        <div>
            <div className='counterContainer'>
                {sproutCount >= 5
                        ?
                        <div>
                            <button className='counterButton' onClick={() => {
                                setPlantedSprouts(plantedSprouts +=1)
                                setSproutCount(sproutCount -= 5)
                                setFarmActive(true)
                            }}>
                                Plant a sprout!!!!!
                            </button>
                        </div>
                        : null
                    }
            </div>
            
        </div>
    );
};

export default Farm;