import React from 'react';
import BeanCounter from "./components/BeanCounter";
import {useState} from 'react';
import CounterText from './components/CounterText';
import EatAllBeans from './components/EatAllBeans';
import OnGround from './components/OnGround';
import Farm from './components/Farm';
import Sword from './components/Sword';


const BeanMain = () => {

    // counters 
    //bean count for clicking generate bean
    //sprout count for buying a sprout
    // ground count for throwing beans on the ground
    //planted sprout for planting a bought sprout into the farm which will become active  
    
    let [beanCount, setBeanCount] = useState(0);
    let [sproutCount, setSproutCount] = useState(0);
    let [beansConsumed, setBeansConsumed] = useState(0);
    let [groundCount, setGroundCount] = useState(0);
    let [plantedSprouts, setPlantedSprouts] = useState(0);
    let [swordCount, setSwordCount] = useState(0)
    

     // active text dependancies

    let [onGroundActive, setOnGroundActive] = useState(false)
    let [healthActive, setHealthActive] = useState(false)
    let [farmActive, setFarmActive] = useState(false)
    let [sproutActive, setSproutActive] = useState(false)
    let [swordActive, setSwordActive] = useState(false);
    
    // clear counter dependancies

    let [clearCounter, setClearCounter] = useState(false)

    

    return (
        <div>
            <h1>ULTIMATE BEAN EXPERIENCE</h1> 
            <div className='container'>
                <div className='row'>
                    <div className='Buttons'>
                        <BeanCounter beanCount={beanCount}
                                    setBeanCount={setBeanCount} 
                                    sproutCount={sproutCount} 
                                    setSproutCount={setSproutCount} 
                                    sproutActive={sproutActive} 
                                    setSproutActive={setSproutActive} />           
                        
                        <EatAllBeans beanCount={beanCount} 
                                    setBeanCount={setBeanCount} 
                                    beansConsumed={beansConsumed} 
                                    setBeansConsumed={setBeansConsumed} 
                                    healthActive={healthActive} 
                                    setHealthActive={setHealthActive}/>          

                        <OnGround beanCount={beanCount}
                                    setBeanCount={setBeanCount}
                                    groundCount={groundCount} 
                                    setGroundCount={setGroundCount}
                                    onGroundActive={onGroundActive}
                                    setOnGroundActive={setOnGroundActive}
                                    clearCounter={clearCounter}
                                    setClearCounter= {setClearCounter} />

                        <Farm beanCount={beanCount}
                                    setBeanCount={setBeanCount} 
                                    sproutCount={sproutCount} 
                                    setSproutCount={setSproutCount} 
                                    plantedSprouts={plantedSprouts}
                                    setPlantedSprouts={setPlantedSprouts}
                                    farmActive={farmActive}
                                    setFarmActive={setFarmActive} />

                        <Sword beanCount={beanCount}
                                    setBeanCount={setBeanCount}
                                    swordCount={swordCount}
                                    setSwordCount={setSwordCount}
                                    swordActive={swordActive}
                                    setSwordActive={setSwordActive} />
                    </div>
                </div>
            </div>

            <CounterText beanCount={beanCount} 
                        sproutActive={sproutActive}
                        sproutCount={sproutCount} 
                        setSproutCount={setSproutCount} 
                        healthActive={healthActive} 
                        beansConsumed={beansConsumed}
                        clearCounter={clearCounter}
                        onGroundActive={onGroundActive}
                        groundCount={groundCount} 
                        plantedSprouts={plantedSprouts}
                        farmActive={farmActive}
                        swordCount={swordCount} 
                        swordActive={swordActive}/>
                    

        </div>
    );

} 

export default BeanMain; 

//used use state to set bean counters
// used inline if statements to determine if a button should be shown
// need to have a throw on the ground buttoN

//imported use effect. set the value as false. if true, using the on click function then more details should show