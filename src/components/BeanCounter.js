
import React from 'react';

const BeanCounter = (props) => {

    let beanCount = props.beanCount;
    let setBeanCount = props.setBeanCount;
    let sproutCount = props.sproutCount
    let setSproutCount = props.setSproutCount
    let setSproutActive = props.setSproutActive
  
    return (
        <div>           
            <div className='counterContainer'>
                <button className='counterButton' onClick={() => setBeanCount(beanCount +=1)}>
                    Generate Beans!!!!
                </button>
            </div>
            <div className='counterContainer'>
                {beanCount >= 5 //should be 25
                    ?   
                    <div>
                        <button className='counterButton' onClick={() => { 
                            setBeanCount(beanCount -=10); 
                            setSproutCount(sproutCount+=1);
                            setSproutActive(true)
                        }}>
                            Buy Bean Sprout
                        </button>
                    </div>
                    :   null 
                } 
            </div>



        </div>
                              
    )
}

export default BeanCounter;