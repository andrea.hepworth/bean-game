import React from 'react'; 
import {useEffect} from "react";

const Sword = (props) => {

    let beanCount = props.beanCount;
    let setBeanCount = props.setBeanCount;

    let swordActive = props.swordActive;
    let setSwordActive = props.setSwordActive;
    let swordCount = props.swordCount;
    let setSwordCount = props.setSwordCount;

    useEffect(() => {
        console.log('sword active!');
    }, [swordActive]);

    return(
        <div>
            <div>
                {beanCount >= 200 ? 
                    <button onClick={() => {setSwordActive(true); setBeanCount(beanCount -200); setSwordCount(swordCount +=1) }}>
                        Buy a sword!
                    </button>
                    : null
                }
            </div>
        </div>
    )

}
export default Sword;