import React from 'react';
import farmicon from './farmIcon.jpg';

const CounterText = (props) => {

    let beanCount = props.beanCount;
    let sproutCount = props.sproutCount;
    let beansConsumed = props.beansConsumed;
    let sproutActive = props.sproutActive;
    let healthActive = props.healthActive;
    let onGroundActive = props.onGroundActive;
    let groundCount = props.groundCount;
    let clearCounter = props.clearCounter;
    let plantedSprouts = props.plantedSprouts;
    let farmActive = props.farmActive;
    let swordCount = props.swordCount;
    let swordActive = props.swordActive;


    return (
        <div>
            <div className='counterText'>
                <p>You have {beanCount} beans!</p>
            </div>
            <div className='counterText'>           
                {sproutActive 
                    ?
                    <div>
                        <p> You bought {sproutCount} bean sprouts</p>
                    </div>
                    : null }
            </div> 
            <div className='counterText'>
                {healthActive 
                    ? 
                    <p>You have eaten {beansConsumed} beans</p>
                    : null}
            </div>
            <div className='counterText'>
                {onGroundActive 
                    ?
                    <div>
                        <p> You threw {groundCount} beans on the ground</p>
                    </div>
                    : null }
            </div>
            <div className='counterText'>
                {clearCounter 
                    ? 
                    <div>
                        <p>A crow came and ate all your beans :(</p>
                    </div> 
                    : null }
            </div>
            <div className='counterText'>
                {farmActive ?
                <div>
                    <p> You have planted {plantedSprouts} sprouts </p>
                    <div className='farmhouse'>
                        <img src={farmicon} alt="here is ur bean farm hehehehee" />
                        <p>Bean Farm</p>
                        <p>Planted sprouts: {plantedSprouts}</p>
                        <p>Current Bean Production: {plantedSprouts} per second</p>
                    </div>               
                </div>
                : null }
            </div>
            <div>
            {swordActive ? 
                <p> You bought a sword! Sword level: {swordCount}</p>
                : null
            }

        </div>
        </div>
    );
};

export default CounterText;